"LAMMPS_Braid_wl_with_swap_Lk31.txt": example of LAMMPS file to run simulations with a constant pulling force and no torque applied to the braid. 
Integration of vDNA happens through the fix "bond/swap". 
The simulation stops just after the second integration.

"equil_wrings_nl10_ls40_Lk31.txt": input file called by the LAMMPS code.
The braid has been prepared with catenation number = 31. 
There are 10 equilibrated vDNA rings, each one composed by 40 beads.

"fix_bond_swap_modified.cpp" & "fix_bond_swap_modified.h": modified c++ code and header file
to include when compiling LAMMPS. The files have been modified to allow bond swaps between 
the two strands and vDNA rings.