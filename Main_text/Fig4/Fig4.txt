#First column=bin centre
#Second column=histogram height for Ca=22. Histograms are related to the contour distance between two consecutive integrations.
#Third column=histogram height for Ca=31
#Fourth column=histogram height for Ca=36
#Fifth column=error associated to the fourth column

20  0.00607798  0.00539216  0.0057377  0.00020229
60  0.00848624  0.00906863  0.00819672  5.3945e-05
100  0.00504587  0.00514706  0.0045082  0.000139355
140  0.00275229  0.00281863  0.00368852  0.000188805
180  0.00194954  0.00220588  0.00273224  0.00043904
220  0.000688073  0.000367647  0.000136612  0.000145349
260  0  0  0  0
